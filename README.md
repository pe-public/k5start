k5start
-------

k5start is used to maintain a Kerberos ticket-granting ticket for a 
service.  This implementation uses systemd, and also the k5start program.  
You'll need to ensure that you have both installed already.  This code also 
requires Puppet's stdlib and systemd.

* `$name` (the implicit parameter): This is the name of the service that will be 
created.
* `$ensure`: set to 'present' if you want the service setup; set to 'absent'
to remove the service. No other values are recognized.
OPTIONAL: defaults to 'present'
* `$name`: the name of the systemd service. This will be the name
used to start and stop the servce.
* `$ticket_file`: the location where the kerberos ticket that k5start
creates should be placed.
* `$keytab`: full path to the keytab file.
* `$principal`: in some cases the keytab file may contain more than one
principal, in which case you may need to specify explicitly the
principal you want to use. 
* `$description`: The text description put into the systemd .service file.
* `$start_before`: This is a list of one or more systemd unit names.  
* `$owner`: By default, the credentials cache file is owned by root.
* `$group`: By default, the credentials cache file is owned by the root group.
* `$mode`: By default, the credentials cache file has permissions 0600: 

## Examples

```
systemd_k5start { 'k5start-ldap':
  keytab      => '/etc/ldap/ldap-localhost.keytab',
  ticket_file => '/var/run/ldap_service_localhost.tkt',
}
```
