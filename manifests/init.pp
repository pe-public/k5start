# k5start is used to maintain a Kerberos ticket-granting ticket for a 
# service.  This implementation uses systemd, and also the k5start program.  
# You'll need to ensure that you have both installed already.  This code also 
# requires Puppet's stdlib.

# $name (the implicit parameter): This is the name of the service that will be 
# created.
#
# $ensure: set to 'present' if you want the service setup; set to 'absent'
#  to remove the service. No other values are recognized.
#  OPTIONAL: defaults to 'present'
#
# $name: the name of the systemd service. This will be the name
#   used to start and stop the servce.
#   REQUIRED
#   Example: If the service_name is 'ldap-sugal', then you would use
#   'systemctl start ldap-sugal' to start the servuce.
#
# $ticket_file: the location where the kerberos ticket that k5start
#   creates should be placed.
#   REQUIRED
#   Example: '/var/run/ldap_service_localhost.tkt'
#
# $keytab: full path to the keytab file.
#   OPTIONAL: defaults to '/etc/krb5.keytab'
#
# $principal: in some cases the keytab file may contain more than one
#   principal, in which case you may need to specify explicitly the
#   principal you want to use. E.g., on the LDAP servers the
#   /etc/krb5.keytab file contains both
#   host/ldap-jessie1.stanford.edu@stanford.edu and
#   ldap/ldap-jessie1.stanford.edu@stanford.edu
#   If you wanted to specify ldap/ldap-jessie1.stanford.edu use
#     $principal => 'ldap/ldap-jessie1.stanford.edu'.
#   OPTIONAL: defaults to the first principal in the keytab file
#
# $description: The text description put into the systemd .service file.
#   OPTIONAL: defaults to "keep ticket file $ticket_file refreshed"
#
# $start_before: This is a list of one or more systemd unit names.  The systemd
#   unit file that is created will have the provided unit names put into the 
#   "Before" field (which means this service should start before the others), 
#   and the "RequiredBy" field (which means this service must start before the 
#   others).  This parameter takes a string (containing a single systemd unit 
#   name), or an array of unit names.
#   OPTIONAL
#   Fun fact: We use both "Before" and "RequiredBy" because the latter is only 
#   used when `systemctl enable` is used.
#   WARNING: If you use this parameter, you need to make sure that your service
#   instance sets the 'enable' parameter to true.  If you don't do that, then
#   systemd will not register the dependency, and it'll be possible to start
#   one service without the other!
#
# $owner: By default, the credentials cache file is owned by root.  If you
#   would like the credentials cache to be owned by someone else, use this 
#   parameter.  Normally the default (root) should be left alone, and
#   the $group and $mode parameters should be used instead.
#
# $group: By default, the credentials cache file is owned by the root group.
#   In many cases, you will want to change this to a different group, such as
#   "www-data".  Note that you will also need to set a new $mode.
#
# $mode: By default, the credentials cache file has permissions 0600: Only
#   readable and writeable by root.  Normally you will want to change this to
#   0660, to give the group access to read and write.  This must be a 
#   three-digit octal number, with an optional leading zero, and you are
#   not allowed to set the execute bit.
#
# IMPORTANT: This define does not setup a service resource, it merely
# creates the systemd unit file. You need to setup the service yourself.
#
#  EXAMPLES:
#
#   systemd_k5start { 'k5start-ldap':
#     keytab      => '/etc/ldap/ldap-localhost.keytab',
#     ticket_file => '/var/run/ldap_service_localhost.tkt',
#   }
#
#   # We have to define the service ourselves:
#   service { 'k5start-ldap':
#     ensure  => 'running',
#     enable  => true,
#     require => Systemd_K5start['k5start-ldap'],
#   }
#
define k5start(
  Enum['present', 'absent'] $ensure = 'present',
  String $description = undef,
  String $keytab = '/etc/krb5.keytab',
  String $ticket_file = undef,
  String $principal = undef,
  Array[String] $start_before = [],
  String $owner = 'root',
  String $group = 'root',
  String $mode = '0600',
) {
  $service_name = "k5start_${name}"

  case $ensure {
    'present': {
      $service_ensure = 'running'
      $service_enable = true
    }
    'absent': {
      $service_ensure = 'stopped'
      $service_enable = false
    }
  }

  if ($ticket_file == undef) {
    fail('ticket_file parameter missing')
  }

  if ($description == undef) {
    $description_use = "keep ticket file ${ticket_file} refreshed"
  } else {
    $description_use = $description
  }

  if ($principal == undef) {
    $principal_option = "-U"
  } else {
    $principal_option = "-u $principal"
  }

  # Make sure we have a valid mode string, and fail if we don't.
  validate_re($mode, '^0?[0246]{3}$',
              '$mode must be a three-digit file mode, with no exec permissions')

  if !defined(Package['kstart']) {
    package {'kstart': ensure => present }
  }

  systemd::service { $service_name:
    ensure       => $ensure,
    description  => $description_use,
    type         => 'simple',
    before_units => $start_before,
    requiredby   => $start_before,
    execstart    => "/usr/bin/k5start $principal_option -f $keytab -k $ticket_file -l 10h -K 30 -m $mode -o $owner -g $group",
    restart      => 'on-failure',
    restart_sec  => '5s',
    require      => Package['kstart'],
  }

  # run the service
  service { $service_name:
    ensure     => $service_ensure,
    enable     => $servcie_enable,
    hasstatus  => true,
    hasrestart => true,
    require    => Systemd::Service[$service_name],
  }

  # We want to reload the systemd daemon on any change to the unit file.
  exec { "k5start_${name}_restart_systemd":
    command     => 'systemctl daemon-reload',
    path        => '/bin',
    subscribe   => Systemd::Service[$service_name],
    refreshonly => true,
  }
}
